Tale of Snail
Hello, you are a snail.
This is your tale.
You have one mission.
To reach the finish line before the hare. 
Good luck, don't disappoint.


Do you accept your mission? 
->yes
  let's begin
->no
   snail's are too cool for you anyways
 

Choose your snail shell: [b]lue or [p]ink
Enter your name 
->b
   blue
-> p
   pink
-> none
   ugly
Welcome to your life as a snail, + name +  I love your  + color +  shell, it really suits you :)
The race will begin in 3...2...1!

You start your journey. Do you choose [w]inding road or [s]lippery slope?
-> w
   Aww. The winding road takes longer for you, a snail, than slippery slopes
-> s
   Congrats! you are a snail so slippery slopes allow your slug self to go faster

Do you choose to take a [b]reak and rest up, or [s]peed up?
-> s
   You tire out and are forced to rest. Should have rested while you had the chance.

A stranger approaches and offers a strange drink, [t]ake it or [n]o?
-> t
    It was coffee which made you crash from no sleep
    You're behind. Write a message to encourage yourself to do better

Do you take [a]viation alley, or [s]wamp way
-> a
   You are too behind. The Hare finishes before you even leave aviation alley.\nYou lose! :(
   (END)
-> s
   The swamp enhances your slug abilities. You are seconds away from the finish line...
 
You have a choice. Do you attempt to [f]inish before the hare or [l]et him win because it will be good for his low self esteem
-> f
   The hare is faster and you look bad for being mean >:(
   (END)
-> l
   Awww you're sweet. You win! :)
    (END)
-> n
    Instead of drinking it you took a refreshing nap. \nYou're ahead! :)

Do you take [a]viation alley, or [s]wamp way?
-> a
   You cannot fly so you trail behind :(

You are seconds away from the finish line... You see a perched bird and right next to it you see the hare. Do you [t]rip the hare or [r]ide the bird
 -> t
     Cheater! The hare trips, and out of spite crushes you under his foot.\nYou lose! :(
      (END)
-> r
    The Bird Takes you to the finish line, milliseconds before the hare.\nYou win! :)
     (END)
-> s
    The swamp enhances your slug abilities.\nYou finish before the Hare and win the race! :)")
     (END)

-> b
    Good! You got enough rest and have doubled your speed naturally.
   
A stranger approaches and offers a strange drink, [t]ake it or [n]o?
-> t
    You get a caffeine rush and crash
You're behind. Write a message to encourage yourself to do better

Do you take [a]viation alley, or [s]wamp way
-> a
    You are too behind. The Hare finishes before you even leave aviation alley.\nYou lose! :(
     (END)
-> s
    The swamp enhances your slug abilities. You are seconds away from the finish line...

You have a choice. Do you attempt to [f]inish before the hare or [l]et him win because it will be good for his low self esteem?
-> f
    The hare is faster and you look bad for being mean.\nYou lose! :(

Do you [a]pologize, or [s]tand your ground
-> a
   Congratulations, your kindness alots you the victory
   (END)
-> s
    Your unsportsmanship puts you last. You lose
    (END)

-> l
     Awww you're sweet.\nYou win! :)
     (END)
-> n
    You kept a steady course.\nYou're ahead! :)

Do you take [a]viation alley, or [s]wamp way?
-> a
    You cannot fly so you trail behind :(

You are seconds away from the finish line... You see a perched bird and right next to it you see the hare. Do you [t]rip the hare or [r]ide the bird
-> t
     Cheater! The hare trips, and out of spite crushes you under his foot.\nYou lose! :(
      (END)
-> r
    The Bird Takes you to the finish line, milliseconds before the hare.\nYou win! :)")
     (END)
-> s
    The swamp enhances your slug abilities.\nYou finish before the Hare and win the race! :)
     (END)





