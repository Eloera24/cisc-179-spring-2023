# Welcome to CISC 179: Python Programming

I'm Hobson Lane. My goal is to help each one of you become a successful Python programmer. 

You will be using this Canvas Learning Management System (LMS) and [Runestone.Academy](Runestone.Academy) to manage all your assignments, exercises, and discussions. You will need to complete one Module (set of assignments) per week.

## Get started:

1. Sign up for the free Runstone.Academy interactive [Fundamentals of Python Programming](https://runestone.academy/ns/books/published/sdccd_mesa_college_cs179_spring23/index.html). Enter the course name **sdccd_mesa_college_cs179_spring23** when you sign up.
2. Work your way through Chapter 1 of the Runestone.Academy book
3. Add my [office hours](https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=NzlyODhma2R2NW41MTdwMjIwZmExaG5qZjlfMjAyMzAzMDRUMTgwMDAwWiBjX25sb25wbjRpYzExbTJkcGs5MHNiY2YwN25rQGc&amp;tmsrc=c_nlonpn4ic11m2dpk90sbcf07nk%40group.calendar.google.com&amp;scp=ALL) to your calendar:
  * Mondays at 6:30 PM <a target="_blank" href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=NWx2ZWtlMXUzNHVsN3FiNjFtYTNjamplODNfMjAyMzAyMjhUMDIzMDAwWiBjX25sb25wbjRpYzExbTJkcGs5MHNiY2YwN25rQGc&amp;tmsrc=c_nlonpn4ic11m2dpk90sbcf07nk%40group.calendar.google.com&amp;scp=ALL"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>
  * Wednesdays at 6 PM <a target="_blank" href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=M3VibDg3aG5ycXBoOTdnYnRscmtnMG41cW9fMjAyMzAyMDlUMDIwMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&amp;tmsrc=hobson%40totalgood.com&amp;scp=ALL"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>
  * Thursdays at 5:00 PM <a target="_blank" href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=MmF0cHF2ZDM0azhyYzVuY2c2dHRmNGN0bmZfMjAyMzAyMTBUMDEwMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&amp;tmsrc=hobson%40totalgood.com&amp;scp=ALL"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>
  * Saturdays at 10:00 AM <a target="_blank" href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=MmF0cHF2ZDM0azhyYzVuY2c2dHRmNGN0bmZfMjAyMzAyMTBUMDEwMDAwWiBob2Jzb25AdG90YWxnb29kLmNvbQ&amp;tmsrc=hobson%40totalgood.com&amp;scp=ALL"><img border="0" src="https://www.google.com/calendar/images/ext/gc_button1_en.gif"></a>
4. Browse to the course [Syllabus](https://gitlab.com/tangibleai/community/cs179/-/blob/main/Table%20of%20Contents%20-%20Foundations%20of%20Python%20Programming.md)


## Optional: have fun with Python

* Download Kenneth Lambert's free e-book [Fundamentals of Python](https://www.dropbox.com/s/9u5jgy0ksidklvu/Fundamentals%20of%20Python%20First%20Programs%20-%202nd%20Ed.pdf?dl=1)
* [EdaBit](https://edabit.com/tutorial/python) - Lessons and hints and interactive interpretters
* [Python Anywhere](https://www.pythonanywhere.com/registration/register/beginner/) - you can even deploy a Python web application (website) here
* [Interactive Python Debugger](https://pythontutor.com/python-debugger.html#) - A debugger visualizes your code as it processes it one line at a time

### Optional (Advanced): Get a `git` account

* Create a free [GitLab.com](gitlab.com) account
* Fork this `git` repo where you will find answers to homeworks and other useful bits of info: [gitlab.com/tangibleai/community/cs179](https://gitlab.com/tangibleai/community/cs179#welcome-to-cisc-179-python-programming)

